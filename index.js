import {
  getAllProducts,
  getProductById,
  generateNewProduct,
} from "./productService.js";

import express from "express";
const app = express();
const port = 8080;
const baseUrl = "/api";
const baseProduct = baseUrl + "/productos";

app.use(express.json())

app.get(baseProduct + "/listar", async (req, res) => {
  /**
   * Devuelve array de productos
   */
  const products = getAllProducts();
  if (products.length > 0) {
    res.send({ products });
  } else {
    res.status(400).send({ error: "No hay productos cargados" });
  }
});

app.get(baseProduct + "/listar/:id([0-9]+)", async (req, res) => {
  /**
   * Devuelve producto por id
   */
  const productId = Number(req.params.id);
  const product = getProductById(productId);

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
});

app.post(baseProduct + "/guardar/", (req, res) => {
  /**
   * Devuelve el producto incorporado
   */
  const { title, price, thumbnail } = req.body;

  const product = generateNewProduct(title, price, thumbnail);
  res.send(product);
});

app.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});
