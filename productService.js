import { generateRandomNumber, generateRandomNumberFloat } from "./utils.js";
const products = [];

export const getAllProducts = () => products;

export const generateNewProduct = (title, price, thumbnail ) => {
  // hago lenght + 1 para que los id empiezen desde 1 y no desde 0
  const product = generateProduct(products.length + 1, title, price, thumbnail );
  products.push(product);
  return product;
};

export const getProductById = (id) => {
  const product = products.find((p) => p.id == id);
  return product;
};

function generateProduct(id, title, price, thumbnail ) {
  return {
    id,
    title,
    price,
    thumbnail,
  };
}
